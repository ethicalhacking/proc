#!/bin/bash

DEV  = `sudo ls /dev`
PROC = `sudo ls /proc`

echo "The /dev content is:\n "
for d in $DEV
do
    echo $d
done

echo "\nThe /proc content is:\n"
for p in $PROC
do
    echo $p
done
